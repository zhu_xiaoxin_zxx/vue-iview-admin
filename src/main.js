import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "@/styles/index.scss"; // global css
import "./permission";
import ViewUI from "view-design";
import "view-design/dist/styles/iview.css";
import "./icons"; // icon
import service from "./server";
import apis from "./api";

Vue.config.productionTip = false;
Vue.prototype.$http = service;
Vue.prototype.APIS = apis;

Vue.use(ViewUI);

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
