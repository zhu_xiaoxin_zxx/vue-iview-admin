import axios from "axios";
import { Message } from "view-design";

const service = axios.create({
  timeout: 5000,
  withCredentials: true,
});

service.interceptors.request.use(
  (config) => {
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

service.interceptors.response.use(
  (response) => {
    const res = response.data;
    if (res.code !== 0) {
      Message.error(res.message || "Error");
      return Promise.reject(new Error(res.message || "Error"));
    } else {
      return res;
    }
  },
  (error) => {
    Message.error(error.message || "Error");
    return Promise.reject(error);
  }
);

export default service;
